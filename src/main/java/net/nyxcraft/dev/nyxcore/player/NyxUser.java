package net.nyxcraft.dev.nyxcore.player;

import net.nyxcraft.dev.ndb.mongodb.dao.UserDAO;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationHistory;
import net.nyxcraft.dev.ndb.mongodb.entities.User;
import org.bson.types.ObjectId;

import java.util.UUID;

public class NyxUser {

    private ObjectId id;
    private UUID uuid;
    private Rank rank;
    private long joinServerTime;
    private long playTime;
    private int gems;
    private int shards;
    private UserNetworkPreferences preferences;
    private ModerationHistory moderationHistory;

    public NyxUser(User user) {
        this.uuid = UUID.fromString(user.uuid);
        this.id = user.id;
        this.rank = Rank.getRankFromString(user.rank);
        this.playTime = user.playTime;
        this.joinServerTime = System.currentTimeMillis();
        this.gems = user.gems;
        this.shards = user.shards;
        this.preferences = new UserNetworkPreferences(user);
        this.moderationHistory = user.moderationHistory;
    }

    public ObjectId getId() {
        return id;
    }

    public Rank getRank() {
        return rank;
    }

    /**
     * Retrieves a player's profile from the database.
     * This method should be used sparingly and only when needed.
     *
     * @return
     */
    public User getProfile() {
        return UserDAO.getInstance().get(id);
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public long getJoinServerTime() {
        return joinServerTime;
    }

    public long getPlayTime() {
        return playTime;
    }

    public UserNetworkPreferences getPreferences() {
        return preferences;
    }

    public UUID getUuid() {
        return uuid;
    }

    public int getGems() {
        return gems;
    }

    public void setGems(int gems) {
        this.gems = gems;
    }

    public int getShards() {
        return shards;
    }

    public void setShards(int shards) {
        this.shards = shards;
    }

    public ModerationHistory getModerationHistory() {
        return moderationHistory;
    }
}
