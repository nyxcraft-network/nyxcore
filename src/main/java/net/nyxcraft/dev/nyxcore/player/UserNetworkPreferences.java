package net.nyxcraft.dev.nyxcore.player;

import net.nyxcraft.dev.ndb.mongodb.entities.User;

public class UserNetworkPreferences {

    private boolean playersVisible;
    private boolean chatEnabled;
    private boolean acceptingPrivateMessages;
    private boolean filterFoulLanguage;

    private transient boolean dirty = false;

    public UserNetworkPreferences(User user) {
        this.playersVisible = user.userSettings.playersVisible;
        this.chatEnabled = user.userSettings.chatEnabled;
        this.acceptingPrivateMessages = user.userSettings.acceptPrivateMessages;
        this.filterFoulLanguage = user.userSettings.filterFoulLanguage;
    }

    public boolean isPlayersVisible() {
        return playersVisible;
    }

    public void setPlayersVisible(boolean playersVisible) {
        this.playersVisible = playersVisible;
        this.dirty = true;
    }

    public boolean isChatEnabled() {
        return chatEnabled;
    }

    public void setChatEnabled(boolean chatEnabled) {
        this.chatEnabled = chatEnabled;
        this.dirty = true;
    }

    public boolean isAcceptingPrivateMessages() {
        return acceptingPrivateMessages;
    }

    public void setAcceptingPrivateMessages(boolean acceptingPrivateMessages) {
        this.acceptingPrivateMessages = acceptingPrivateMessages;
        this.dirty = true;
    }

    public boolean isFilterFoulLanguage() {
        return filterFoulLanguage;
    }

    public void setFilterFoulLanguage(boolean filterFoulLanguage) {
        this.filterFoulLanguage = filterFoulLanguage;
        this.dirty = true;
    }

    public boolean isDirty() {
        return dirty;
    }
}
