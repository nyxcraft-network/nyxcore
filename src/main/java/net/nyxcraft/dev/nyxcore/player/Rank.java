package net.nyxcraft.dev.nyxcore.player;

import org.bukkit.ChatColor;

public enum Rank {

    DEFAULT("", ChatColor.GRAY),
    HERO("Hero", ChatColor.GREEN),
    TITAN("Titan", ChatColor.RED),
    OLYMPIAN("Olympian", ChatColor.DARK_AQUA),
    YOUTUBER("Youtuber", ChatColor.DARK_RED),
    HELPER("Helper", ChatColor.AQUA),
    MODERATOR("Moderator", ChatColor.DARK_GREEN),
    SRMODERATOR("SrMod", ChatColor.DARK_GREEN),
    ADMINISTRATOR("Admin", ChatColor.GOLD),
    DEVELOPER("Dev", ChatColor.LIGHT_PURPLE),
    OWNER("Owner", ChatColor.DARK_PURPLE);

    public String prefix;
    public ChatColor color;

    Rank(String prefix, ChatColor color) {
        this.prefix = prefix;
        this.color = color;
    }

    public static Rank getRankFromString(String r) {
        for (Rank rank : Rank.values()) {
            if (rank.name().equalsIgnoreCase(r)) {
                return rank;
            }
        }

        return DEFAULT;
    }

    public static boolean matchFound(String r) {
        for (Rank rank : Rank.values()) {
            if (rank.name().equalsIgnoreCase(r)) {
                return true;
            }
        }

        return false;
    }

}
