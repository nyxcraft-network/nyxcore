package net.nyxcraft.dev.nyxcore.chat;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class MessageFormatter {

    public static void sendUsageMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l»&6&l» &4&lUSAGE&7: &6" + message));
        }
    }

    public static void sendSuccessMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&l»&2&l» &a&lSUCCESS&7: &a" + message));
        }
    }

    public static void sendErrorMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l»&4&l» &4&lERROR&7: &c" + message));
        }
    }

    public static void sendGeneralMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7&l»&8&l» &7" + message));
        }
    }

    public static void sendInfoMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l»&6&l» &6" + message));
        }
    }

    public static void sendAnnouncement(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&d&l»&5&l» &4&lANNOUNCEMENT&7: &d" + message));
        }
    }

}
