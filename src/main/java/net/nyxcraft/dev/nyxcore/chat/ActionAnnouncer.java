package net.nyxcraft.dev.nyxcore.chat;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxutils.chat.MessageService;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class ActionAnnouncer extends BukkitRunnable {

    private static ActionAnnouncer instance;
    private boolean active = true;
    private List<String> messages;
    private int lastMessage = 0;
    private int sent = 0;
    private int interval = 5;
    private int task;

    public ActionAnnouncer(List<String> messages, int interval) {
        instance = this;
        this.messages = messages;
        this.interval = interval;
        this.task = Bukkit.getScheduler().scheduleSyncRepeatingTask(NyxCore.getInstance(), this, 20, 20);
    }

    @Override
    public void run() {
        if (active) {
            if (messages != null && messages.size() > 0) {
                String message;
                if (sent < interval) {
                    sent++;
                    message = messages.get(lastMessage);
                } else {
                    sent = 0;
                    message = lastMessage == messages.size() ?  messages.get(0) : messages.get(lastMessage++);

                    if (lastMessage == messages.size()) {
                        lastMessage = 0;
                    }
                }
                MessageService.actionAnnouncement(message);
            }
        }
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setMessages(List<String> messages) {
        this.lastMessage = 0;
        this.sent = 0;
        this.messages = messages;
    }

    public static void cleanup() {
        if (instance != null) {
            Bukkit.getScheduler().cancelTask(instance.task);
        }
    }
}
