package net.nyxcraft.dev.nyxcore.world;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class SerializeableLocation {

    String world;
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    public SerializeableLocation() {}

    public SerializeableLocation(Location location) {
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
        this.world = location.getWorld().getName();
    }

    public Location getLocation() {
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

}
