package net.nyxcraft.dev.nyxcore.world;

import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class WorldBorderManager implements Listener {

    private String world;
    private int x, y, z;
    private float pitch, yaw;
    int radius;

    public WorldBorderManager(JavaPlugin plugin, Location center, int radius) {
        update(center, radius);
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void check(PlayerMoveEvent event) {
        Location location = event.getTo();
        if (location.getWorld().getName().equalsIgnoreCase(world)) {
            if (location.getX() > x + radius || location.getX() < x - radius || location.getZ() > z + radius || location.getZ() < z - radius) {
                MessageFormatter.sendGeneralMessage(event.getPlayer(), "You have reached the end of the world. You will be teleported to the world spawn.");
                event.getPlayer().teleport(new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch));
            }
        }
    }

    public void update(Location center, int radius) {
        this.world = center.getWorld().getName();
        this.x = center.getBlockX();
        this.y = center.getBlockY();
        this.z = center.getBlockZ();
        this.pitch = center.getPitch();
        this.yaw = center.getYaw();
        this.radius = radius;
    }
}
