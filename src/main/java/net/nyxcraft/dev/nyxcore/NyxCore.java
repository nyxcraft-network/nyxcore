package net.nyxcraft.dev.nyxcore;

import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.entities.NetworkSettings;
import net.nyxcraft.dev.nyxcore.command.CommandListener;
import net.nyxcraft.dev.nyxcore.command.modules.CommonCommands;
import net.nyxcraft.dev.nyxcore.command.modules.CommonWorldCommands;
import net.nyxcraft.dev.nyxcore.command.modules.moderation.*;
import net.nyxcraft.dev.nyxcore.command.modules.player.UserCommands;
import net.nyxcraft.dev.nyxcore.listener.ChatListener;
import net.nyxcraft.dev.nyxcore.listener.ConnectionListener;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.ui.MenuAPI;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class NyxCore extends JavaPlugin {

    private static NyxCore instance;
    private static Map<UUID, NyxUser> users = new HashMap<>();
    private static MenuAPI api;
    private static NetworkSettings networkSettings;

    public void onEnable() {
        instance = this;
        api = new MenuAPI(this);
        registerCommands();
        registerListeners();
        loadNetworkSettings();
    }

    private void loadNetworkSettings() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(NyxCore.getInstance(), () -> networkSettings = DataAPI.getNetworkSettings(), 20, 20 * 60 * 5);
    }

    public void registerCommands() {
        CommandListener.setup(this);
        new UserCommands();
        new BanCommands();
        new KickCommands();
        new TeleportCommands();
        new StatCommands();
        new CommonWorldCommands();
        new CommonCommands();
        new MuteCommands();
        new ChatCommands();
    }

    public void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new ConnectionListener(), this);
        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
    }

    public static NyxCore getInstance() {
        return instance;
    }

    public static boolean containsUser(UUID uuid) {
        return users.containsKey(uuid);
    }

    public static NyxUser getUser(UUID uuid) {
        return users.get(uuid);
    }

    public static void addUser(UUID uuid, NyxUser user) {
        users.put(uuid, user);
    }

    public static void removeUser(UUID uuid) {
        users.remove(uuid);
    }

    public static Map<UUID, NyxUser> getUsers() {
        return users;
    }

    public static NetworkSettings getNetworkSettings() {
        return networkSettings;
    }

    public static void setNetworkSettings(NetworkSettings networkSettings) {
        NyxCore.networkSettings = networkSettings;
    }
}
