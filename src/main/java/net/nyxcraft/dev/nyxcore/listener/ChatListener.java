package net.nyxcraft.dev.nyxcore.listener;

import net.md_5.bungee.api.ChatColor;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.player.UserNetworkPreferences;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ChatListener implements Listener {

    private static HashMap<UUID, Long> lastMessageTime = new HashMap<>();
    private static HashMap<UUID, Integer> mutedPlayers = new HashMap<>();
    private static Set<UUID> avoidSpamFilter = new HashSet<>();
    private boolean split = false;

    @EventHandler(priority = EventPriority.HIGHEST)
    public void getOnChatHighest(AsyncPlayerChatEvent event) {
        if (split) {
            event.setCancelled(true);
            split = false;
            return;
        }

        split = true;

        Set<Player> filteredPlayers = new HashSet<>();
        Set<Player> staff = new HashSet<>();
        NyxUser sender = NyxCore.getUser(event.getPlayer().getUniqueId());
        UserNetworkPreferences prefs = sender.getPreferences();
        String filterSymbol = "";
        if (prefs.isFilterFoulLanguage()) {
            filterSymbol = ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "F" + ChatColor.DARK_GRAY + "] ";
        } else {
            filterSymbol = ChatColor.DARK_GRAY + "[" + ChatColor.RED + "F" + ChatColor.DARK_GRAY + "] ";

        }

        // control variable to avoid endless loop
        boolean filtered = false;
        String message = "";
        String staffMessage = "";
        String[] words = event.getMessage().split(" ");
        for (String word : words) {
            if (NyxCore.getNetworkSettings().filteredWords.contains(word.toLowerCase())) {
                filtered = true;
                message += "**** ";
                if (sender.getRank().ordinal() >= Rank.HELPER.ordinal()) {
                    staffMessage += ChatColor.RED + word + ChatColor.WHITE + " ";
                } else {
                    staffMessage += ChatColor.RED + word + ChatColor.GRAY + " ";
                }
            } else {
                message += word + " ";
                staffMessage += word + " ";
            }
        }
        message = message.trim();
        staffMessage = staffMessage.trim();

        if (filtered) {
            for (Player pl : event.getRecipients()) {
                NyxUser user = NyxCore.getUser(pl.getUniqueId());
                UserNetworkPreferences preferences = user.getPreferences();

                if (user.getRank().ordinal() >= Rank.HELPER.ordinal()) {
                    staff.add(pl);
                    continue;
                }

                if (preferences.isFilterFoulLanguage()) {
                    filteredPlayers.add(pl);
                }
            }
            event.getRecipients().removeAll(filteredPlayers);
            event.getRecipients().removeAll(staff);

            final String finalMessage = message;
            final String finalStaffMessage = staffMessage;
            // Filters messages so the **** message gets sent
            avoidSpamFilter.add(event.getPlayer().getUniqueId());
            AsyncPlayerChatEvent e = new AsyncPlayerChatEvent(false, event.getPlayer(), finalMessage, filteredPlayers);
            Bukkit.getPluginManager().callEvent(e);
            for (Player pl : filteredPlayers) {
                pl.sendMessage(String.format(e.getFormat(), e.getPlayer().getDisplayName(), e.getMessage()));
            }
            filteredPlayers.clear();

            // Filters message so staff sees the word highlighted
            avoidSpamFilter.add(event.getPlayer().getUniqueId());
            AsyncPlayerChatEvent staffChat = new AsyncPlayerChatEvent(false, event.getPlayer(), finalStaffMessage, staff);
            Bukkit.getPluginManager().callEvent(staffChat);
            for (Player pl : staff) {
                pl.sendMessage(String.format(filterSymbol + staffChat.getFormat(), staffChat.getPlayer().getDisplayName(), staffChat.getMessage()));
            }
        } else {
            staff.clear();
            for (Player pl : event.getRecipients()) {
                NyxUser user = NyxCore.getUser(pl.getUniqueId());
                UserNetworkPreferences preferences = user.getPreferences();

                if (user.getRank().ordinal() >= Rank.HELPER.ordinal()) {
                    staff.add(pl);
                }
            }

            event.getRecipients().removeAll(staff);

            // extra message for Staff so they see the [F] in front of the name at every message
            avoidSpamFilter.add(event.getPlayer().getUniqueId());
            AsyncPlayerChatEvent staffChat = new AsyncPlayerChatEvent(false, event.getPlayer(), event.getMessage(), staff);
            Bukkit.getPluginManager().callEvent(staffChat);
            for (Player pl : staff) {
                pl.sendMessage(String.format(filterSymbol + staffChat.getFormat(), staffChat.getPlayer().getDisplayName(), staffChat.getMessage()));
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onChatNormal(AsyncPlayerChatEvent event) {
        if (mutedPlayers.containsKey(event.getPlayer().getUniqueId())) {
            MessageFormatter.sendErrorMessage(event.getPlayer(), "You are muted.");
            event.setCancelled(true);
            return;
        }

        Long time = System.currentTimeMillis();
        Long lastSpoken = lastMessageTime.get(event.getPlayer().getUniqueId());

        if (lastSpoken == null) {
            lastSpoken = 0L;
        }

        if (lastSpoken + 1000L > time && !avoidSpamFilter.contains(event.getPlayer().getUniqueId())) {
            MessageFormatter.sendErrorMessage(event.getPlayer(), "You are sending messages to quickly.");
            event.setCancelled(true);
            return;
        }

        // remove him now again so he can't spam
        if (lastSpoken + 1000L > time && avoidSpamFilter.contains(event.getPlayer().getUniqueId())) {
            avoidSpamFilter.remove(event.getPlayer().getUniqueId());
        }


        lastMessageTime.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
    }

    public static HashMap<UUID, Integer> getMutedPlayers() {
        return mutedPlayers;
    }
    public static Set<UUID> getAvoidSpamFilter() {
        return avoidSpamFilter;
    }

    public static void addMute(NyxUser user) {
        ChatListener.getMutedPlayers().put(user.getUuid(), Bukkit.getScheduler().scheduleSyncDelayedTask(NyxCore.getInstance(),
                () -> ChatListener.getMutedPlayers().remove(user.getUuid()),
                (user.getModerationHistory().activeMute.expiration.getTime() - System.currentTimeMillis()) / 50));
    }
}
