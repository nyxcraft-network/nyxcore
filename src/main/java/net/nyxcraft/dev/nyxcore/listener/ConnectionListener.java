package net.nyxcraft.dev.nyxcore.listener;

import net.nyxcraft.dev.dsr.bukkit.DSRBukkit;
import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationEntry;
import net.nyxcraft.dev.ndb.mongodb.entities.User;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.event.LoginSuccessEvent;
import net.nyxcraft.dev.nyxcore.menu.NetworkPreferencesMenu;
import net.nyxcraft.dev.nyxcore.moderation.ModerationUtils;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        User user = DataAPI.initUser(event.getUniqueId(), event.getName(), event.getAddress().getHostAddress());

        if (user == null) {
            return;
        }

        boolean banned = ModerationUtils.isBanned(user);

        if (banned) {
            ModerationEntry ban = user.moderationHistory.activeBan;

            if (ban.expiration == null) {
                event.setKickMessage(ChatColor.translateAlternateColorCodes('&', "\n&cYou have been banned from this network!"
                        + "\n&6Reason: &c" + ban.reason));
            } else {
                long duration = ban.expiration.getTime() - System.currentTimeMillis();

                event.setKickMessage(ChatColor.translateAlternateColorCodes('&', "\n&cYou are temporarily banned from the network!"
                        + "\n&6Reason: &c" + ban.reason
                        + "\n&6You will be unbanned in &c" + Utils.convertToFormattedTime(duration)));
            }

            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            return;
        }

        user.lastServer = DSRBukkit.getInstance().getDynamicSettings().getName();
        DataAPI.setLastServer(event.getUniqueId(), user.lastServer);
        NyxUser nyxUser = new NyxUser(user);
        NyxCore.addUser(event.getUniqueId(), nyxUser);
        Bukkit.getPluginManager().callEvent(new LoginSuccessEvent(nyxUser));
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onLoginSuccess(LoginSuccessEvent event) {
        if (event.getProfile().getModerationHistory().activeMute != null) {
            ChatListener.addMute(event.getProfile());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (ChatListener.getMutedPlayers().containsKey(event.getPlayer().getUniqueId())) {
            ChatListener.getMutedPlayers().remove(event.getPlayer().getUniqueId());
        }

        if (ChatListener.getAvoidSpamFilter().contains(event.getPlayer().getUniqueId())) {
            ChatListener.getAvoidSpamFilter().remove(event.getPlayer().getUniqueId());
        }

        event.setQuitMessage(null);
        DataAPI.setTimeStats(event.getPlayer().getUniqueId(), System.currentTimeMillis() - NyxCore.getUser(event.getPlayer().getUniqueId()).getJoinServerTime(), System.currentTimeMillis());
        NetworkPreferencesMenu.cleanup(event.getPlayer());
        NyxCore.removeUser(event.getPlayer().getUniqueId());
    }

}
