package net.nyxcraft.dev.nyxcore.moderation;

import net.nyxcraft.dev.ndb.mongodb.entities.ModerationEntry;
import net.nyxcraft.dev.ndb.mongodb.entities.User;

import java.util.Date;

public class ModerationUtils {

    public static boolean isBanned(User user) {
        ModerationEntry entry = user.moderationHistory.activeBan;
        if (entry != null) {
            if (entry.expiration != null) {
                if (new Date(System.currentTimeMillis()).before(entry.expiration)) {
                    return true;
                }
            } else {
                return true;
            }
        }

        return false;
    }

}
