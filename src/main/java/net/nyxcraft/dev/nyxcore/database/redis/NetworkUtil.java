package net.nyxcraft.dev.nyxcore.database.redis;

import net.nyxcraft.dev.dsr.bukkit.DSRBukkit;
import net.nyxcraft.dev.dsr.redis.pubsub.NetTask;
import org.bukkit.entity.Player;

public class NetworkUtil {

    public static void send(Player player, String server) {
        NetTask.withName("send")
                .withArg("player", player.getName())
                .withArg("server", server)
                .send("send", DSRBukkit.getInstance().getRedis());
    }

}
