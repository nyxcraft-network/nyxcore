package net.nyxcraft.dev.nyxcore.command.modules.moderation;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class TeleportCommands {

    public TeleportCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "tp", Rank.MODERATOR, TeleportCommands::tp);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "tphere", Rank.MODERATOR, TeleportCommands::tpHere);
    }

    public static void tp(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendErrorMessage(sender, "/tp <name>");
            return;
        }

        Player player = Bukkit.getPlayer(args[0]);
        if (player == null) {
            MessageFormatter.sendErrorMessage(sender, args[0] + " is not online.");
            return;
        }

        sender.teleport(player);
        MessageFormatter.sendInfoMessage(sender, "You have been teleported to " + player.getName());
        MessageFormatter.sendInfoMessage(player, sender.getName() + " has teleported to you.");
    }

    public static void tpHere(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/tphere <name>");
            return;
        }

        Player player = Bukkit.getPlayer(args[0]);
        if (player == null) {
            MessageFormatter.sendErrorMessage(sender, args[0] + " is not online");
            return;
        }

        player.teleport(sender);
        MessageFormatter.sendInfoMessage(player, "You have been teleported to " + sender.getName());
        MessageFormatter.sendInfoMessage(sender, player.getName() + " has teleported to you.");
    }

}
