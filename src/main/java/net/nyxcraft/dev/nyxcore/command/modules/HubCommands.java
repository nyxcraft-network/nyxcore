package net.nyxcraft.dev.nyxcore.command.modules;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class HubCommands {

    public HubCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "fly", Rank.HERO, HubCommands::fly);
    }

    public static void fly(Player sender, String[] args) {
        if (sender.isFlying()) {
            sender.setAllowFlight(false);
            sender.setFlying(false);
            MessageFormatter.sendInfoMessage(sender, "You are no longer flying.");
        } else {
            sender.setAllowFlight(true);
            sender.setFlying(true);
            MessageFormatter.sendInfoMessage(sender, "You are now flying.");
        }
    }

}
