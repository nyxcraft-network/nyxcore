package net.nyxcraft.dev.nyxcore.command.modules.moderation;

import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.entities.User;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.player.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class StatCommands {

    public StatCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "seen", Rank.SRMODERATOR, StatCommands::seen);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "ontime", Rank.MODERATOR, StatCommands::onTime);
    }

    public static void seen(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/seen <name>");
            return;
        }

        final String name = args[0];
        Player player = Bukkit.getPlayer(name);
        if (player != null) {
            if (player != sender) {
                MessageFormatter.sendErrorMessage(sender, "That player is on the same server as you.");
            } else {
                MessageFormatter.sendErrorMessage(sender, "Go take a look in the mirror.");
            }
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(NyxCore.getInstance(), () -> {
            User user = DataAPI.retrieveUserByName(name);
            if (user == null) {
                MessageFormatter.sendErrorMessage(sender, "Stats could not be retrieved for " + name);
                return;
            }

            if (user.playTime == 0) {
                MessageFormatter.sendErrorMessage(sender, ChatColor.GOLD + name + " &chas yet to play on the network.");
                return;
            }

            MessageFormatter.sendInfoMessage(sender, name + " was last online on " + formatDate(user.lastOnline), "They were last seen on " + user.lastServer + " with ip " + user.ip);
        });
    }

    public static void onTime(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/ontime <name>");
            return;
        }

        final String name = args[0];
        Player player = Bukkit.getPlayer(name);
        if (player != null) {
            NyxUser user = NyxCore.getUser(player.getUniqueId());
            long playTime = user.getPlayTime() + (System.currentTimeMillis() - user.getJoinServerTime());
            MessageFormatter.sendInfoMessage(sender, name + "'s total time on nyxcraft: " + formatTime(playTime));
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(NyxCore.getInstance(), () -> {
            User user = DataAPI.retrieveUserByName(name);
            if (user == null) {
                MessageFormatter.sendErrorMessage(sender, "Stats could not be retrieved for " + name);
                return;
            }
            MessageFormatter.sendInfoMessage(sender, name + "'s total time on nyxcraft: " + formatTime(user.playTime));
        });
    }

    public static String formatDate(long time) {
        final Date date = new Date(time);
        final String LEGACY_FORMAT = "EEE MMM dd hh:mm:ss zzz yyyy";
        final SimpleDateFormat sdf = new SimpleDateFormat(LEGACY_FORMAT);
        final TimeZone utc = TimeZone.getTimeZone("EST");
        sdf.setTimeZone(utc);
        return sdf.format(date);
    }

    public static String formatTime(long playTime) {
        return String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(playTime), TimeUnit.MILLISECONDS.toSeconds(playTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(playTime)));
    }

}
