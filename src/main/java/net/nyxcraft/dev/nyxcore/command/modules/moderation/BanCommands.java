package net.nyxcraft.dev.nyxcore.command.modules.moderation;

import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.dao.UserDAO;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationAction;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationEntry;
import net.nyxcraft.dev.ndb.mongodb.entities.User;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class BanCommands {

    public BanCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "ban", Rank.MODERATOR, BanCommands::ban);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "tempban", Rank.MODERATOR, BanCommands::tempban);
        CommandRegistry.registerUniversalCommand(NyxCore.getInstance(), "unban", Rank.SRMODERATOR, BanCommands::unban);
    }

    public static void ban(Player sender, String[] args) {
        if (args.length < 2) {
            MessageFormatter.sendUsageMessage(sender, "/ban <player> <reason>");
            return;
        }

        String player = args[0];
        String reason = args[1];

        if (args.length > 2) {
            for (int i = 2; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (target != null) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been banned for: &6" + reason));
        }

        DataAPI.addModerationEntry(new ModerationEntry(user.uuid, sender.getUniqueId().toString(), ModerationAction.BAN, reason));
        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas banned by &e&l" + sender.getName(), "&cfor &6&l" + reason);
        }
    }

    public static void tempban(Player sender, String[] args) {
        if (args.length < 3) {
            MessageFormatter.sendUsageMessage(sender, "/tempban <player> <#m|#h|#d> <reason>");
            return;
        }

        String player = args[0];
        String time = args[1];

        String reason = args[2];

        if (args.length > 3) {
            for (int i = 3; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (target != null) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been tempbanned for: &6" + reason));
        }

        try {
            DataAPI.addModerationEntry(new ModerationEntry(user.uuid, sender.getUniqueId().toString(), ModerationAction.BAN, reason, Utils.getTime(time).longValue()));
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/ban <player> <#m|#h|#d> <reason>");
            return;
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas tempbanned by &e&l" + sender.getName(), "&cfor &6&l" + reason + " &7(" + time + ")");
        }
    }

    public static void unban(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/unban <player>");
            return;
        }

        String player = args[0];

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        user.moderationHistory.activeBan = null;
        UserDAO.getInstance().save(user);
        MessageFormatter.sendSuccessMessage(sender, ChatColor.GOLD + player + " &4has been unbanned.");
    }
}
