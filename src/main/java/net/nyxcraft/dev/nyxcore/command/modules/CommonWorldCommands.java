package net.nyxcraft.dev.nyxcore.command.modules;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class CommonWorldCommands {

    public CommonWorldCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "tpw", Rank.SRMODERATOR, CommonWorldCommands::teleportWorld);
    }

    public static void teleportWorld(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/tpw <world>");
            return;
        }

        String name = args[0];
        World world = Bukkit.getWorld(name);

        if (world == null) {
            MessageFormatter.sendErrorMessage(sender, "That world does not exist.");
            return;
        }

        sender.teleport(world.getSpawnLocation());
        MessageFormatter.sendSuccessMessage(sender, "You have been teleported to " + name);
    }

}
