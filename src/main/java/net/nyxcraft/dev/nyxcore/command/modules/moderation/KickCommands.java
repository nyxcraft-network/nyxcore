package net.nyxcraft.dev.nyxcore.command.modules.moderation;

import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationAction;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationEntry;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class KickCommands {

    public KickCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "kick", Rank.HELPER, KickCommands::kick);
    }

    public static void kick(Player sender, String[] args) {
        if (args.length < 2) {
            MessageFormatter.sendUsageMessage(sender, "/kick <player> <reason>");
            return;
        }

        String player = args[0];
        String reason = args[1];

        if (args.length > 2) {
            for (int i = 2; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        if (target.isOnline()) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been kicked for: &6" + reason));
        } else {
            return;
        }

        DataAPI.addModerationEntry(new ModerationEntry(target.getUniqueId().toString(), sender.getUniqueId().toString(), ModerationAction.KICK, reason));
        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas kicked by &e&l" + sender.getName(), "&cfor &6&l" + reason);
        }
    }

}
