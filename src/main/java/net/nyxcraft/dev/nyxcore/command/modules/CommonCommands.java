package net.nyxcraft.dev.nyxcore.command.modules;

import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.database.redis.NetworkUtil;
import net.nyxcraft.dev.nyxcore.listener.ChatListener;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.player.UserNetworkPreferences;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class CommonCommands {

    // sender, receiver
    private static HashMap<UUID, UUID> lastMessages = new HashMap<>();

    public CommonCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "message", CommonCommands::message);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "msg", CommonCommands::message);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "m", CommonCommands::message);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "tell", CommonCommands::message);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "hub", CommonCommands::hub);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "lobby", CommonCommands::hub);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "reply", CommonCommands::reply);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "r", CommonCommands::reply);
    }

    public static void message(Player player, String[] args) {
        if (args.length < 2) {
            MessageFormatter.sendUsageMessage(player, "/msg [player] [message]");
            return;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (p.getName().startsWith(args[0])) {
                    target = p;
                }
            }
        }

        if (target == null) {
            MessageFormatter.sendErrorMessage(player, "Player not found");
            return;
        } else {
            if (ChatListener.getMutedPlayers().containsKey(player.getUniqueId())) {
                MessageFormatter.sendErrorMessage(player, "You are muted.");
                return;
            }

            if (NyxCore.getUser(player.getUniqueId()).getRank().ordinal() < Rank.HELPER.ordinal()) {
                UserNetworkPreferences preferences = NyxCore.getUser(target.getUniqueId()).getPreferences();
                if (!preferences.isAcceptingPrivateMessages()) {
                    MessageFormatter.sendErrorMessage(player, target.getName() + " is not accepting private messages right now.");
                    return;
                }
            }
        }

        lastMessages.put(target.getUniqueId(), player.getUniqueId());
        lastMessages.put(player.getUniqueId(), target.getUniqueId());
        String[] messageArray = new String[args.length - 1];
        System.arraycopy(args, 1, messageArray, 0, args.length - 1);
        String message = Arrays.toString(messageArray).replace("[", "").replace("]", "").replace(",", "");
        target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&5[&7" + player.getName() + "&5 -> &7You&5] &f" + message));
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&5[&7You &5-> &7" + target.getName() + "&5] &f" + message));
    }

    public static void reply(Player player, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(player, "/r [message]");
            return;
        }

        Player target = Bukkit.getPlayer(lastMessages.get(player.getUniqueId()));
        if (target == null) {
            MessageFormatter.sendErrorMessage(player, "You have no one to whom you can reply.");
            return;
        } else {
            if (ChatListener.getMutedPlayers().containsKey(player.getUniqueId())) {
                MessageFormatter.sendErrorMessage(player, "You are muted.");
                return;
            }
            if (NyxCore.getUser(player.getUniqueId()).getRank().ordinal() < Rank.HELPER.ordinal()) {
                UserNetworkPreferences preferences = NyxCore.getUser(target.getUniqueId()).getPreferences();
                if (!preferences.isAcceptingPrivateMessages()) {
                    MessageFormatter.sendErrorMessage(player, target.getName() + " is not accepting private messages right now.");
                    return;
                }
            }
        }
        lastMessages.put(target.getUniqueId(), player.getUniqueId());
        String message = Arrays.toString(args).replace("[", "").replace("]", "").replace(",", "");
        target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&5[&7" + player.getName() + "&5 -> &7You&5] &f" + message));
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&5[&7You &5-> &7" + target.getName() + "&5] &f" + message));
    }

    public static void hub(Player player, String[] args) {
        NetworkUtil.send(player, "lobby");
        MessageFormatter.sendInfoMessage(player, "Teleporting you to the lobby.");
    }

}
