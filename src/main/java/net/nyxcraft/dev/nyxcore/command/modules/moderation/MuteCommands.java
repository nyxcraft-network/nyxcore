package net.nyxcraft.dev.nyxcore.command.modules.moderation;

import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.dao.UserDAO;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationAction;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationEntry;
import net.nyxcraft.dev.ndb.mongodb.entities.User;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.listener.ChatListener;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MuteCommands {

    public MuteCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "mute", Rank.HELPER, MuteCommands::tempmute);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "tempmute", Rank.HELPER, MuteCommands::tempmute);
        CommandRegistry.registerUniversalCommand(NyxCore.getInstance(), "unmute", Rank.HELPER, MuteCommands::unmute);
    }

    public static void tempmute(Player sender, String[] args) {
        if (args.length < 3) {
            MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
            return;
        }

        String player = args[0];
        String time = args[1];
        String reason = args[2];

        if (args.length > 3) {
            for (int i = 3; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        long duration;
        try {
            duration = Utils.getTime(time).longValue();
            if (duration == -1) {
                MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
                return;
            }
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
            return;
        }

        ModerationEntry entry = new ModerationEntry(user.uuid.toString(), sender.getUniqueId().toString(), ModerationAction.MUTE, reason, duration);
        DataAPI.addModerationEntry(entry);

        if (target != null) {
            NyxCore.getUser(target.getUniqueId()).getModerationHistory().activeMute = entry;
            ChatListener.addMute(NyxCore.getUser(target.getUniqueId()));
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas tempmuted by &e&l" + sender.getName(), "&cfor &6&l" + reason + " &7(" + time + ")");
        }
    }

    public static void unmute(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/unmute <player>");
            return;
        }

        String player = args[0];

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (target != null) {
            ChatListener.getMutedPlayers().remove(target.getUniqueId());
        }

        user.moderationHistory.activeMute = null;
        UserDAO.getInstance().save(user);
        MessageFormatter.sendSuccessMessage(sender, ChatColor.GOLD + player + " &4has been unmuted.");
    }

}
