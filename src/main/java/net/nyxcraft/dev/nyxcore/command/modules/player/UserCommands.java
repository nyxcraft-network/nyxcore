package net.nyxcraft.dev.nyxcore.command.modules.player;

import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.dao.UserDAO;
import net.nyxcraft.dev.ndb.mongodb.entities.User;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.event.SetPlayerRankEvent;
import net.nyxcraft.dev.nyxcore.menu.NetworkPreferencesMenu;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class UserCommands {

    public UserCommands() {
        CommandRegistry.registerUniversalCommand(NyxCore.getInstance(), "setrank", Rank.ADMINISTRATOR, UserCommands::setRank);
        CommandRegistry.registerUniversalCommand(NyxCore.getInstance(), "preloaduser", Rank.ADMINISTRATOR, UserCommands::preLoadUser);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "settings", UserCommands::settings);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "givegems", Rank.ADMINISTRATOR, UserCommands::giveGems);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "giveshards", Rank.ADMINISTRATOR, UserCommands::giveShards);
    }

    public static void setRank(CommandSender sender, String[] args) {
        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(sender, "/setrank <player> <rank>");
            return;
        }

        String name = args[0];
        String rank = args[1];

        if (Rank.matchFound(rank) == false) {
            MessageFormatter.sendErrorMessage(sender, "That is not a valid rank.");
            return;
        }

        Rank r = Rank.getRankFromString(rank);
        Player player = Bukkit.getPlayer(name);
        NyxUser u = null;
        if (player != null) {
            u = NyxCore.getUser(player.getUniqueId());
        }
        User user = u != null ? u.getProfile() : DataAPI.retrieveUserByName(name);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (u != null) {
            u.setRank(r);
            MessageFormatter.sendInfoMessage(Bukkit.getPlayer(UUID.fromString(user.uuid)), "Your rank has been set to " + r.name());
        }

        user.rank = r.name();
        UserDAO.getInstance().save(user);
        MessageFormatter.sendSuccessMessage(sender, name + "'s rank has been set to " + r.name());
        Bukkit.getPluginManager().callEvent(new SetPlayerRankEvent(user));
    }

    public static void preLoadUser(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/preloaduser <player>");
            return;
        }

        final String name = args[0];
        Player player = Bukkit.getPlayer(name);
        NyxUser u = null;
        if (player != null) {
            u = NyxCore.getUser(player.getUniqueId());
        }
        if (u != null) {
            MessageFormatter.sendErrorMessage(sender, "That user already exists in the database!");
            return;
        }

        User user = DataAPI.retrieveUserByName(name);
        if (user != null) {
            MessageFormatter.sendErrorMessage(sender, "That user already exists in the database!");
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(NyxCore.getInstance(), () -> {
            List<String> names = new ArrayList<String>() {{
                add(name);
            }};
            UUIDFetcher fetcher = new UUIDFetcher(names);
            Map<String, UUID> uniqueIds = null;

            try {
                uniqueIds = fetcher.call();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            if (uniqueIds == null || uniqueIds.isEmpty()) {
                MessageFormatter.sendErrorMessage(sender, "No player could be found using the name " + name);
                return;
            }

            for (Map.Entry<String, UUID> entry : uniqueIds.entrySet()) {
                if (entry.getKey().equalsIgnoreCase(name)) {
                    DataAPI.initUser(entry.getValue(), entry.getKey(), null);
                    MessageFormatter.sendSuccessMessage(sender, "The player has successfully been preloaded into the database.");
                    return;
                }
            }

            MessageFormatter.sendErrorMessage(sender, "We were unable to add that user to the database.");
        });
    }

    public static void giveGems(Player sender, String[] args) {
        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(sender, "/givegems <player> <#>");
            return;
        }

        String name = args[0];
        int gems;
        try {
            gems = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/givegems <player> <#>");
            return;
        }

        Player player = Bukkit.getPlayer(name);
        NyxUser u = null;
        if (player != null) {
            u = NyxCore.getUser(player.getUniqueId());
        }
        User user = u != null ? u.getProfile() : DataAPI.retrieveUserByName(name);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (u != null) {
            u.setGems(u.getGems() + gems);
            MessageFormatter.sendInfoMessage(player, "You have received " + gems + " gems.");
        }

        DataAPI.updateGems(UUID.fromString(user.uuid), gems);
        MessageFormatter.sendSuccessMessage(sender, "You gave " + user.name + " " + gems + " gems.");
    }

    public static void giveShards(Player sender, String[] args) {
        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(sender, "/giveshards <player> <#>");
            return;
        }

        String name = args[0];
        int shards;
        try {
            shards = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/giveshards <player> <#>");
            return;
        }

        Player player = Bukkit.getPlayer(name);
        NyxUser u = null;
        if (player != null) {
            u = NyxCore.getUser(player.getUniqueId());
        }
        User user = u != null ? u.getProfile() : DataAPI.retrieveUserByName(name);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (u != null) {
            u.setShards(u.getShards() + shards);
            MessageFormatter.sendInfoMessage(player, "You have received " + shards + " shards.");
        }

        DataAPI.updateShards(UUID.fromString(user.uuid), shards);
        MessageFormatter.sendSuccessMessage(sender, "You gave " + user.name + " " + shards + " shards.");
    }

    public static void settings(Player player, String[] args) {
        NetworkPreferencesMenu.getUserPreferences(player).openMenu(player);
    }

}
