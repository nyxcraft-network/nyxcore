package net.nyxcraft.dev.nyxcore.command.modules.moderation;

import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.dao.NetworkSettingsDAO;
import net.nyxcraft.dev.ndb.mongodb.entities.NetworkSettings;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.event.AnnouncementsUpdatedEvent;
import net.nyxcraft.dev.nyxcore.player.Rank;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatCommands {

    public ChatCommands() {
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "addword", Rank.MODERATOR, ChatCommands::addword);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "addannouncement", Rank.ADMINISTRATOR, ChatCommands::addAnnouncement);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "clearannouncements", Rank.ADMINISTRATOR, ChatCommands::clearAnnouncements);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "listannouncements", Rank.ADMINISTRATOR, ChatCommands::listAnnouncements);
        CommandRegistry.registerPlayerCommand(NyxCore.getInstance(), "removeannouncement", Rank.ADMINISTRATOR, ChatCommands::removeAnnouncements);
    }

    public static void addword(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/addword <word>");
            return;
        }

        NetworkSettings settings = DataAPI.getNetworkSettings();
        settings.filteredWords.add(args[0].toLowerCase());
        NyxCore.setNetworkSettings(settings);
        DataAPI.addFilteredWord(args[0].toLowerCase());
        MessageFormatter.sendSuccessMessage(player, args[0] + " has been added to the filter.");
    }

    public static void addAnnouncement(Player player, String[] args) {
        if (args.length < 2) {
            MessageFormatter.sendUsageMessage(player, "/addannouncement <gametype> <announcement>");
            return;
        }

        NetworkSettings settings = DataAPI.getNetworkSettings();
        String gametype = args[0].toLowerCase();
        String message = StringUtils.join(Arrays.copyOfRange(args, 1, args.length), " ");
        if (settings.announcements.containsKey(gametype.toLowerCase())) {
            settings.announcements.get(gametype).add(message);
        } else {
            settings.announcements.put(gametype, new ArrayList<String>() {{
                add(message);
            }});
        }

        NetworkSettingsDAO.getInstance().save(settings);
        MessageFormatter.sendSuccessMessage(player, "Announcement has been added.");
        Bukkit.getPluginManager().callEvent(new AnnouncementsUpdatedEvent(settings));
    }

    public static void clearAnnouncements(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/clearannouncements <gametype>");
            return;
        }

        NetworkSettings settings = DataAPI.getNetworkSettings();
        String gametype = args[0].toLowerCase();
        if (settings.announcements.containsKey(gametype.toLowerCase())) {
            settings.announcements.remove(gametype);
        } else {
            MessageFormatter.sendErrorMessage(player, "There are no announcements configured for that gametype.");
            return;
        }

        NetworkSettingsDAO.getInstance().save(settings);
        MessageFormatter.sendSuccessMessage(player, "Announcements have been clear.");
        Bukkit.getPluginManager().callEvent(new AnnouncementsUpdatedEvent(settings));
    }

    public static void listAnnouncements(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/listannouncements <gametype>");
            return;
        }

        NetworkSettings settings = DataAPI.getNetworkSettings();
        String gametype = args[0].toLowerCase();
        if (settings.announcements.containsKey(gametype.toLowerCase())) {
            List<String> announcements = settings.announcements.get(gametype);
            MessageFormatter.sendGeneralMessage(player, "== Announcements ==");
            for (int i = 0; i < announcements.size(); i++) {
                MessageFormatter.sendGeneralMessage(player, (i + 1) + ": " + announcements.get(i));
            }
        } else {
            MessageFormatter.sendErrorMessage(player, "There are no announcements configured for that gametype.");
        }
    }

    public static void removeAnnouncements(Player player, String[] args) {
        if (args.length != 2 || !StringUtils.isNumeric(args[1])) {
            MessageFormatter.sendUsageMessage(player, "/listannouncements <gametype> <messageID>");
            return;
        }

        NetworkSettings settings = DataAPI.getNetworkSettings();
        String gametype = args[0].toLowerCase();
        int id = Integer.parseInt(args[1]);

        if (id < 1) {
            MessageFormatter.sendErrorMessage(player, "The ID must be greater or equals to 1");
            return;
        }
        if (settings.announcements.containsKey(gametype.toLowerCase())) {
            if (settings.announcements.get(gametype).size() < id) {
                MessageFormatter.sendErrorMessage(player, "There is no announcement configured for that ID in the given gamemode.");
                return;
            }
            settings.announcements.get(gametype).remove(id - 1);
            if (settings.announcements.get(gametype).size() == 0) {
                settings.announcements.remove(gametype);
            }
        } else {
            MessageFormatter.sendErrorMessage(player, "There are no announcements configured for that gametype.");
            return;
        }

        NetworkSettingsDAO.getInstance().save(settings);
        MessageFormatter.sendSuccessMessage(player, "Announcement has been removed.");
        Bukkit.getPluginManager().callEvent(new AnnouncementsUpdatedEvent(settings));

    }
}
