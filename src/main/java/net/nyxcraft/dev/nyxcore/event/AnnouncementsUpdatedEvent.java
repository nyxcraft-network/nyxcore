package net.nyxcraft.dev.nyxcore.event;

import net.nyxcraft.dev.ndb.mongodb.entities.NetworkSettings;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class AnnouncementsUpdatedEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private NetworkSettings settings;

    public AnnouncementsUpdatedEvent(NetworkSettings settings) {
        this.settings = settings;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public NetworkSettings getNetworkSettings() {
        return settings;
    }
}
