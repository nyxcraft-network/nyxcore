package net.nyxcraft.dev.nyxcore.event;

import net.nyxcraft.dev.nyxcore.player.NyxUser;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class LoginSuccessEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private NyxUser profile;

    public LoginSuccessEvent(NyxUser profile) {
        this.profile = profile;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public NyxUser getProfile() {
        return profile;
    }
}
