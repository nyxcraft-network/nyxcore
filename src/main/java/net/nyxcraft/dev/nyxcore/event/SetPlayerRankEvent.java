package net.nyxcraft.dev.nyxcore.event;

import net.nyxcraft.dev.ndb.mongodb.entities.User;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SetPlayerRankEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private User profile;

    public SetPlayerRankEvent(User profile) {
        this.profile = profile;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public User getProfile() {
        return profile;
    }
}
