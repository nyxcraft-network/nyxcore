package net.nyxcraft.dev.nyxcore.utils;

import org.bukkit.entity.Player;

public interface Particle 
{
    public String getName();

    public void preformEffect(Player player);

    public boolean canUse(Player player);

    public void setValue(String str);

}
