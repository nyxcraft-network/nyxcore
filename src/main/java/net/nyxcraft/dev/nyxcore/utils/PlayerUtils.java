package net.nyxcraft.dev.nyxcore.utils;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxutils.reflection.EntityHandler;
import net.nyxcraft.dev.nyxutils.reflection.MethodBuilder;
import org.bukkit.entity.Player;

public class PlayerUtils {

    public static void clearArrowsFromPlayer(Player player) {
        Object watcher = EntityHandler.getWatcher(player);

        if (watcher == null) {
            NyxCore.getInstance().getLogger().info("Watcher of entity is null!");
            return;
        }

        new MethodBuilder(watcher.getClass(), "watch", watcher, new Class<?>[] { int.class, Object.class }).invoke(9, (Byte) (byte) 0);
    }

}
