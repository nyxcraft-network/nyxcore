package net.nyxcraft.dev.nyxcore.menu;

import net.nyxcraft.dev.ndb.mongodb.DataAPI;
import net.nyxcraft.dev.ndb.mongodb.entities.UserSettings;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import net.nyxcraft.dev.nyxcore.player.UserNetworkPreferences;
import net.nyxcraft.dev.nyxcore.ui.Menu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class NetworkPreferencesMenu extends Menu {
    private static Map<UUID, NetworkPreferencesMenu> users = new HashMap<>();
    private UserNetworkPreferences preferences;

    private MenuItem vis;
    private MenuItem chat;
    private MenuItem priv;
    private MenuItem filter;

    public NetworkPreferencesMenu(NyxUser data) {
        super(ChatColor.RED + "User Preferences", 6);
        this.preferences = data.getPreferences();
        users.put(data.getUuid(), this);
        init();
    }

    public void init() {
        DyeColor visDye = preferences.isPlayersVisible() ? DyeColor.LIME : DyeColor.GRAY;
        vis = new MenuItem(ChatColor.GOLD + NetworkPreferencesMenuEnum.TOGGLE_PLAYER_VISIBILITY.getSetting(), new MaterialData(Material.INK_SACK), visDye.getDyeData()) {
            @Override
            public void onClick(Player player) {
                if (preferences.isPlayersVisible()) {
                    vis.setData(DyeColor.GRAY.getDyeData());
                    preferences.setPlayersVisible(false);
                } else {
                    vis.setData(DyeColor.LIME.getDyeData());
                    preferences.setPlayersVisible(true);
                }

                NetworkPreferencesMenu.this.updateInventory();
            }
        };

        DyeColor chatDye = preferences.isChatEnabled() ? DyeColor.LIME : DyeColor.GRAY;
        chat = new MenuItem(ChatColor.GOLD + NetworkPreferencesMenuEnum.TOGGLE_CHAT.getSetting(), new MaterialData(Material.INK_SACK), chatDye.getDyeData()) {
            @Override
            public void onClick(Player player) {
                if (preferences.isChatEnabled()) {
                    chat.setData(DyeColor.GRAY.getDyeData());
                    preferences.setChatEnabled(false);
                } else {
                    chat.setData(DyeColor.LIME.getDyeData());
                    preferences.setChatEnabled(true);
                }

                NetworkPreferencesMenu.this.updateInventory();
            }
        };

        DyeColor privDye = preferences.isAcceptingPrivateMessages() ? DyeColor.LIME : DyeColor.GRAY;
        priv = new MenuItem(ChatColor.GOLD + NetworkPreferencesMenuEnum.TOGGLE_PRIVATE_MESSAGING.getSetting(), new MaterialData(Material.INK_SACK), privDye.getDyeData()) {
            @Override
            public void onClick(Player player) {
                if (preferences.isAcceptingPrivateMessages()) {
                    priv.setData(DyeColor.GRAY.getDyeData());
                    preferences.setAcceptingPrivateMessages(false);
                } else {
                    priv.setData(DyeColor.LIME.getDyeData());
                    preferences.setAcceptingPrivateMessages(true);
                }

                NetworkPreferencesMenu.this.updateInventory();
            }
        };

        DyeColor filterDye = preferences.isFilterFoulLanguage() ? DyeColor.LIME : DyeColor.GRAY;
        filter = new MenuItem(ChatColor.GOLD + NetworkPreferencesMenuEnum.TOGGLE_LANGUAGE_FILTER.getSetting(), new MaterialData(Material.INK_SACK), filterDye.getDyeData()) {
            @Override
            public void onClick(Player player) {
                if (preferences.isFilterFoulLanguage()) {
                    filter.setData(DyeColor.GRAY.getDyeData());
                    preferences.setFilterFoulLanguage(false);
                } else {
                    filter.setData(DyeColor.LIME.getDyeData());
                    preferences.setFilterFoulLanguage(true);
                }

                NetworkPreferencesMenu.this.updateInventory();
            }
        };



        this.addMenuItem(NetworkPreferencesMenuEnum.TOGGLE_PLAYER_VISIBILITY.getMenuItem(), NetworkPreferencesMenuEnum.TOGGLE_PLAYER_VISIBILITY.getIndex());
        this.addMenuItem(NetworkPreferencesMenuEnum.TOGGLE_CHAT.getMenuItem(), NetworkPreferencesMenuEnum.TOGGLE_CHAT.getIndex());
        this.addMenuItem(NetworkPreferencesMenuEnum.TOGGLE_PRIVATE_MESSAGING.getMenuItem(), NetworkPreferencesMenuEnum.TOGGLE_PRIVATE_MESSAGING.getIndex());
        this.addMenuItem(NetworkPreferencesMenuEnum.TOGGLE_LANGUAGE_FILTER.getMenuItem(), NetworkPreferencesMenuEnum.TOGGLE_LANGUAGE_FILTER.getIndex());

        this.addMenuItem(vis, NetworkPreferencesMenuEnum.TOGGLE_PLAYER_VISIBILITY.getIndex() + 9);
        this.addMenuItem(chat, NetworkPreferencesMenuEnum.TOGGLE_CHAT.getIndex() + 9);
        this.addMenuItem(priv, NetworkPreferencesMenuEnum.TOGGLE_PRIVATE_MESSAGING.getIndex() + 9);
        this.addMenuItem(filter, NetworkPreferencesMenuEnum.TOGGLE_LANGUAGE_FILTER.getIndex() + 9);

        updateInventory();
    }

    public static NetworkPreferencesMenu getUserPreferences(Player player) {
        NetworkPreferencesMenu menu = users.containsKey(player.getUniqueId()) ? users.get(player.getUniqueId()) : new NetworkPreferencesMenu(NyxCore.getUser(player.getUniqueId()));
        users.put(player.getUniqueId(), menu);
        return menu;
    }

    public static void cleanup(Player player) {
        UserNetworkPreferences prefs = getUserPreferences(player).preferences;
        if (prefs.isDirty()) {
            UserSettings settings = new UserSettings();
            settings.playersVisible = prefs.isPlayersVisible();
            settings.chatEnabled = prefs.isChatEnabled();
            settings.acceptPrivateMessages = prefs.isAcceptingPrivateMessages();
            settings.filterFoulLanguage = prefs.isFilterFoulLanguage();

            DataAPI.updateUserSettings(player.getUniqueId(), settings);
        }

        users.remove(player.getUniqueId());
    }
}

enum NetworkPreferencesMenuEnum {

    TOGGLE_PLAYER_VISIBILITY("Player Visibility", Material.EYE_OF_ENDER, 10),
    TOGGLE_CHAT("Player Chat", Material.BOOK, 13),
    TOGGLE_PRIVATE_MESSAGING("Private Messaging", Material.BOOK_AND_QUILL, 16),
    TOGGLE_LANGUAGE_FILTER("Language Filter", Material.SIGN, 38);

    private String setting;
    private Material item;
    private int index;

    NetworkPreferencesMenuEnum(String setting, Material item, int index) {
        this.setting = setting;
        this.item = item;
        this.index = index;
    }

    public String getSetting() {
        return setting;
    }

    public Material getItem() {
        return item;
    }

    public int getIndex() {
        return index;
    }

    public MenuItem getMenuItem() {
        return new MenuItem(ChatColor.GOLD + setting, new MaterialData(item)) {
            @Override
            public void onClick(Player player) {
                //
            }
        };
    }
}
